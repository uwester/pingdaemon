pingdaemon
==========
Keep track of remote server

Requirements
------------
You need python 3.X.

Setup
-----
    $ git clone git@github.com:unolarsson/pingdaemon.git
    $ cd pingdaemon
    $ virtualenv -p python3 ./virtenv
    $ . ./virtenv/bin/activate
    $ pip install -r requirements.txt
    $ ./pingdaemon.py URL PING_INTERVAL ALARM_FAIL_COUNTER -e alarm.sh -d
