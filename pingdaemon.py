#!./virtenv/bin/python3
import requests
import argparse
import os
import time
import logging

class PingDaemon():
    
    _host = ""
    _interval = 60
    _alarm_count= 10
    _daemon = False
    _fails = 0
    _execute = ""

    def __init__(self, host, interval, alarm_count, execute, daemon):
        logger.info('init {0}, interval: {1}'.format(host, interval))
        self._host = host
        self._interval = interval
        self._alarm_count = alarm_count
        self._execute = execute
        
        if daemon:
            logger.info('daemon mode')
            self._daemon = True

        self.ping()

    def ping(self):
        try:
            r = requests.get(self._host)
            r.raise_for_status()
            logger.info('ping: {}, {}'.format(r.url, r.status_code))
            
        except:
            self._fails += 1
            logger.info('ping failed: {} times, for {}'.format(self._fails, r.url))
            
            if self._fails >= self._alarm_count:
                logger.warning('ALARM!')
                self._fails = 0
            
                if self._execute:
                    logger.info('execute! {}'.format(self._execute))
                    os.system('{0}'.format(self._execute))
           
        time.sleep(self._interval)
        self.ping()

def startDaemon():
    PingDaemon(args.host, args.interval, args.alarm_count, args.execute, True)

if __name__ == "__main__":
    
    parser = argparse.ArgumentParser()
    parser.add_argument("host", type=str, help="What host to ping")
    parser.add_argument("interval", type=int, default=60, help="How often do we want to ping", nargs="?")
    parser.add_argument("alarm_count", type=int, default=10, help="Warn after X faild pings", nargs="?")
    parser.add_argument("-e", "--execute", type=str, help="Execute Alarm", nargs="?")
    parser.add_argument("-d", "--daemon", help="Run as a daemon", action="store_true")
    args = parser.parse_args()
    
    logger = logging.getLogger(__name__)
    logger.setLevel(logging.DEBUG)
    logger.propagate = False
    fh = logging.FileHandler("./pingdaemon.log", "a")
    fh.setLevel(logging.DEBUG)
    logger.addHandler(fh)
    keep_fds = [fh.stream.fileno()]
    
    if args.daemon:
        if os.getuid() != 0:
            logger.error('not root')
            parser.error('Needs to run as root when running as daemon.')
        else:
            from daemonize import Daemonize
            pid = "/tmp/pd.pid"
            daemon = Daemonize(app="PingDaemon", pid=pid, action=startDaemon, keep_fds=keep_fds)
            daemon.start()
    else:
        PingDaemon(args.host, args.interval, args.alarm_count, args.execute, False)
